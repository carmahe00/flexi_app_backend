const express = require('express')
const path = require('path')
const multer = require('multer')
const router = express.Router()

const { fileUploadCategoria, fileUploadProducto, fileUploadConfig } = require('../controllers/uploadFile')
const { validarJWT } = require('../middleware/validar-jwt');
const { validarCampos } = require('../middleware/validar-campos');

/**
 * @param storage destination: indica donde se almacena. filename: indica el nombre del archivo
 */
const storage = multer.diskStorage({
    destination(req, file, cb) {
        if (req.path.includes('productos'))
            cb(null, 'uploads/productos')
        if (req.path.includes('configuracion'))
            cb(null, 'uploads/config')
        else
            cb(null, 'uploads/')
    },
    filename(req, file, cb) {
        cb(null, `${file.fieldname}-${Date.now()}${path.extname(file.originalname)}`)
    }
})

/**
 * método para validar la extensión de la imagen
 * @param {*} file foto que envia desde el front
 * @param {*} cb callback regresa un true si funciona
 */
function checkFileType(file, cb) {
    const filetypes = /jpg|jpeg|png/
    const extname = filetypes.test(path.extname(file.originalname).toLowerCase())
    const mimetype = filetypes.test(file.mimetype)
    if (extname && mimetype)
        return cb(null, true)
    else
        cb('Solo imagenes')
}

/**
 * Tiene todas la configuración del middleware
 */
const upload = multer({
    storage,
    fileFilter: function (req, file, cb) {
        checkFileType(file, cb)
    }
})

/**
 * upload obtiene la informacion de la imagen
 */
router.post('/:id', [
    validarJWT,
    validarCampos,
    upload.single('image')
], fileUploadCategoria)

router.post('/productos/:id', [
    validarJWT,
    validarCampos,
    upload.single('image')
], fileUploadProducto)

router.post('/configuracion/:id', [
    validarJWT,
    validarCampos,
    upload.single('image')
], fileUploadConfig)

module.exports = router