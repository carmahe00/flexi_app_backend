const { Router } = require('express');
const { getCategorias, createCategoria, updateCategoria, getPublicCategorias } = require('../controllers/categorias');
const { check } = require('express-validator');

const { validarJWT } = require('../middleware/validar-jwt');
const { validarCampos } = require('../middleware/validar-campos');

const router = Router();

router.get('/',[
    validarJWT,
    validarCampos
] ,getCategorias)

router.post('/',[
    validarJWT,
    validarCampos
] ,createCategoria)

router.put('/:id',[
    check('image').notEmpty(),
    check('codigo').notEmpty(),
    check('descripcion').notEmpty(),
    check('nombre').notEmpty(),
    validarJWT,
    validarCampos
], updateCategoria)

router.get('/public',getPublicCategorias)

module.exports = router