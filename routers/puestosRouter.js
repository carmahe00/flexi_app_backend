const { Router } = require('express');
const { getPuestos } = require('../controllers/puestos');

const router = Router();

router.get('/',getPuestos)

module.exports = router