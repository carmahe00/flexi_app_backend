const { Router } = require('express');
const { getConfiguracion, updatedConfiguracion } = require('../controllers/configuracion');
const { check } = require('express-validator');

const { validarJWT } = require('../middleware/validar-jwt');
const { validarCampos } = require('../middleware/validar-campos');

const router = Router();

router.get('/', getConfiguracion)



router.put('/:id', [
    check('Nit').notEmpty(),
    check('nombre').notEmpty(),
    check('direccion').notEmpty(),
    check('telefono').notEmpty(),
    check('imagen').notEmpty(),
    validarJWT,
    validarCampos
], updatedConfiguracion)

module.exports = router