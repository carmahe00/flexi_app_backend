const { Router } = require('express');
const { getCobros, createCobro, updatedCobro, getPublicCobros } = require('../controllers/cobros');
const { check } = require('express-validator');

const { validarJWT } = require('../middleware/validar-jwt');
const { validarCampos } = require('../middleware/validar-campos');

const router = Router();

router.get('/:idCategoria', [
    validarJWT,
    validarCampos
], getCobros)

router.post('/:idCategoria', [
    validarJWT,
    validarCampos
], createCobro)

router.put('/:id', [
    check('codigo').notEmpty(),
    check('descripcion').notEmpty(),
    check('nombre').notEmpty(),
    check('valor').notEmpty(),
    check('total').notEmpty(),
    check('impuesto').notEmpty(),
    check('precio1').notEmpty(),
    check('inicial').notEmpty(),
    check('stok').notEmpty(),
    check('inventario').notEmpty(),
    check('image').notEmpty(),
    validarJWT,
    validarCampos
], updatedCobro)

router.get('/public/:idCategoria', getPublicCobros)

module.exports = router