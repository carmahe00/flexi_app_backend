const { Router } = require('express');
const { check } = require('express-validator');

const { createPedido } = require('../controllers/pedidos');
const { validarCampos } = require('../middleware/validar-campos');

const router = Router();

router.post('/',[
    check('detalles').notEmpty(),
    check('cartItems').notEmpty(),
    validarCampos
],createPedido)

module.exports = router