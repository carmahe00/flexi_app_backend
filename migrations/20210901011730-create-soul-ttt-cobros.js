'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('soul_ttt_cobros', {
      id_cobro: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      codigo: {
        type: Sequelize.STRING
      },
      descripcion: {
        type: Sequelize.STRING
      },
      nombre: {
        type: Sequelize.STRING
      },
      valor: {
        type: Sequelize.DECIMAL
      },
      total: {
        type: Sequelize.DECIMAL
      },
      impuesto: {
        type: Sequelize.DECIMAL
      },
      precio1: {
        type: Sequelize.DECIMAL
      },
      inicial: {
        type: Sequelize.DECIMAL
      },
      stok: {
        type: Sequelize.DECIMAL
      },
      inventario: {
        type: Sequelize.INTEGER
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('soul_ttt_cobros');
  }
};