'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.addColumn(
        'soul_inv_grupos', // table name
        'image', // new field name
        {
          type: Sequelize.STRING,
          allowNull: false,
          defaultValue: ''
        },
      )])
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */
  }
};
