const { request, response } = require("express")
const { Soul_servicios_puesto } = require('../models');

const getPuestos = async (req = request, res = response) => {
    try {
        const puestos = await Soul_servicios_puesto.findAll({
            where: {
                id_servicio: 5
            }
        })
        return res.send(puestos)
    } catch (error) {
        console.log(error)
        return res.status(500).json({
            msg: 'Algo salió mal'
        })
    }
}

module.exports = {
    getPuestos
}