const { request, response } = require("express")
const { Soul_setup } = require('../models');

const getConfiguracion = async (req = request, res = response) => {

    try {
        const configuraciones = await Soul_setup.findAll({ limit: 1 })

        return res.send(configuraciones)
    } catch (error) {
        console.log(error)
        return res.status(500).json({
            msg: 'Algo salió mal'
        })
    }
}

const updatedConfiguracion = async (req = request, res = response) => {
    const { id } = req.params
    try {
        const cobro = await Soul_setup.update(req.body, {
            where: {
                Nit: id,
            }
        })
        return res.status(201).send(cobro)
    } catch (error) {
        console.log(error)
        return res.status(500).json({
            msg: 'Algo salió mal'
        })
    }
}


module.exports = {
    updatedConfiguracion,
    getConfiguracion
}