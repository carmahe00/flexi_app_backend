const fs = require('fs');
const { Soul_inv_grupo, Soul_ttt_cobros, Soul_setup } = require('../models');

const { response, request } = require('express');

const fileUploadCategoria = async (req = request, res = response) => {
    const { id } = req.params
    try {
        const soul_inv_grupo = await Soul_inv_grupo.findOne({
            where: {
                id_grupo: id,
            }
        })

        if (soul_inv_grupo.image)
            if (fs.existsSync(`./${soul_inv_grupo.image}`))
                fs.unlinkSync(`./${soul_inv_grupo.image}`);

        soul_inv_grupo.image = `/${req.file.path}`
        await soul_inv_grupo.save();

        res.send(`/${req.file.path}`);
    } catch (error) {
        console.log(error)
        return res.status(500).json({
            msg: 'Algo salió mal'
        })
    }
}

const fileUploadProducto = async (req = request, res = response) => {
    const { id } = req.params
    try {
        const soul_ttt_cobro = await Soul_ttt_cobros.findOne({
            where: {
                id_cobro: id,
            }
        })

        if (soul_ttt_cobro.image)
            if (fs.existsSync(`./${soul_ttt_cobro.image}`))
                fs.unlinkSync(`./${soul_ttt_cobro.image}`);

        soul_ttt_cobro.image = `/${req.file.path}`
        await soul_ttt_cobro.save();

        res.send(`/${req.file.path}`);
    } catch (error) {
        console.log(error)
        return res.status(500).json({
            msg: 'Algo salió mal'
        })
    }
}

const fileUploadConfig = async (req = request, res = response) => {
    const { id } = req.params
    try {
        const soul_setup = await Soul_setup.findOne({
            where: {
                Nit: id,
            }
        })
        
        if (soul_setup.imagen)
            if (fs.existsSync(`./${soul_setup.imagen}`))
                fs.unlinkSync(`./${soul_setup.imagen}`);

        soul_setup.imagen = `/${req.file.path}`
        await soul_setup.save();

        res.send(`/${req.file.path}`);
    } catch (error) {
        console.log(error)
        return res.status(500).json({
            msg: 'Algo salió mal'
        })
    }
}

module.exports = {
    fileUploadCategoria,
    fileUploadProducto,
    fileUploadConfig
}