const { request, response } = require("express")
const { getPagination, getPagingData } = require("../helpers/pagination");
const { Soul_inv_grupo } = require('../models');
const { Op } = require("sequelize");

const getCategorias = async (req = request, res = response) => {
    const { page, size, puesto } = req.query
    let condition = puesto ? { puesto: Sequelize.where(Sequelize.fn('LOWER', Sequelize.col('puesto')), 'LIKE', `%${puesto}%`) } : null
    const { limit, offset } = getPagination(page, size)

    try {
        const soul_inv_grupo = await Soul_inv_grupo.findAndCountAll({
            limit,
            offset,
            where: condition,
        })
        const categorias = getPagingData(soul_inv_grupo, page, limit)
        return res.send(categorias)
    } catch (error) {
        console.log(error)
        return res.status(500).json({
            msg: 'Algo salió mal'
        })
    }
}

const createCategoria = async (req = request, res = response) => {
    try {
        const soul_inv_grupo = await Soul_inv_grupo.create({})

        return res.status(201).send(soul_inv_grupo)
    } catch (error) {
        console.log(error)
        return res.status(500).json({
            msg: 'Algo salió mal'
        })
    }
}

const updateCategoria = async (req = request, res = response) => {
    const { image, codigo, descripcion, nombre } = req.body
    try {
        const categoria = await Soul_inv_grupo.update({ image, codigo, descripcion, nombre }, {
            where: {
                id_grupo: req.params.id,

            }
        })
        return res.status(201).send(categoria)
    } catch (error) {
        console.log(error)
        return res.status(500).json({
            msg: 'Algo salió mal'
        })
    }
}

const getPublicCategorias = async (req = request, res = response) => {
    try {
        const categorias = await Soul_inv_grupo.findAll({
            where: {
                id_grupo: {
                    [Op.ne]: [
                        0
                    ]
                }
            }
        })
        return res.send(categorias)
    } catch (error) {
        console.log(error)
        return res.status(500).json({
            msg: 'Algo salió mal'
        })
    }
}

module.exports = {
    getCategorias,
    createCategoria,
    updateCategoria,
    getPublicCategorias
}