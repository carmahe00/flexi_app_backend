const { request, response } = require("express")
const { Soul_ttt_cobros } = require('../models');
const { Op } = require("sequelize");

const getCobros = async (req = request, res = response) => {
    const { idCategoria } = req.params
    try {
        const soul_ttt_cobros = await Soul_ttt_cobros.findAll({
            where: {
                id_categoria: idCategoria
            }
        })

        return res.send(soul_ttt_cobros)
    } catch (error) {
        console.log(error)
        return res.status(500).json({
            msg: 'Algo salió mal'
        })
    }
}

const createCobro = async (req = request, res = response) => {
    const { idCategoria } = req.params
    try {
        const soul_ttt_cobro = await Soul_ttt_cobros.create({ id_categoria: idCategoria })
        return res.status(201).send(soul_ttt_cobro)
    } catch (error) {
        console.log(error)
        return res.status(500).json({
            msg: 'Algo salió mal'
        })
    }
}

const updatedCobro = async (req = request, res = response) => {
    const { id } = req.params
    try {
        const cobro = await Soul_ttt_cobros.update(req.body, {
            where: {
                id_cobro: id,

            }
        })
        return res.status(201).send(cobro)
    } catch (error) {
        console.log(error)
        return res.status(500).json({
            msg: 'Algo salió mal'
        })
    }
}

const getPublicCobros = async (req = request, res = response) => {
    const { idCategoria } = req.params
    try {
        const soul_ttt_cobros = await Soul_ttt_cobros.findAll({
            where: {
                id_categoria: {
                    [Op.ne]: 0,
                    [Op.eq]:idCategoria
                }

            }
        })

        return res.send(soul_ttt_cobros)
    } catch (error) {
        console.log(error)
        return res.status(500).json({
            msg: 'Algo salió mal'
        })
    }
}

module.exports = {
    getCobros,
    createCobro,
    updatedCobro,
    getPublicCobros
}