const { request, response } = require("express")
const { Soul_pedidos, Soul_pedidosdetalle, Sequelize } = require('../models');

const createPedido = async (req = request, res = response) => {
    const { detalles, cartItems } = req.body
    const { id_servicio, nombre, direccion, telefono, id_puesto } = detalles

    try {
        let soul_pedido = null
        if (id_servicio === 6)
            soul_pedido = await Soul_pedidos.create({ id_servicio, cliente: nombre, direccion, celular: telefono,id_puesto: 10 })
        else
            soul_pedido = await Soul_pedidos.create({ id_servicio, id_puesto })
        
        if (soul_pedido)
            cartItems.forEach(async (item) => {
                await Soul_pedidosdetalle.create({
                    id_pedido: soul_pedido.id_pedido,
                    cantidad: item.count,
                    descripcion: item.sugerencias,
                    id_producto: item.id_cobro,
                    valor: (item.count * item.precio1),
                    nombre: item.nombre
                })
            });

        return res.json(soul_pedido)
    } catch (error) {
        console.log(error)
        return res.status(500).json({
            msg: 'Algo salió mal'
        })
    }
}

module.exports = {
    createPedido
}