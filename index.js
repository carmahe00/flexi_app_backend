const express = require('express');
const { sequelize } = require('./models');
const bodyParser = require('body-parser');
const cors = require('cors');
const path = require('path');

require('dotenv').config();

const app = express()
//CORS
app.use(cors({
    origin: '*'
}));
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))
app.use(express.json())

app.use('/api/users', require('./routers/userRouter'));

app.use('/api/despachos', require('./routers/despachoRouter'));
app.use('/api/pedidos-pendiente', require('./routers/pedidosPRouter'));
app.use('/api/categorias', require('./routers/categoriaRoute'));
app.use('/api/cobros', require('./routers/cobroRouter'))
app.use('/api/upload', require('./routers/uploadRouter'))
app.use('/api/puestos', require('./routers/puestosRouter'))
app.use('/api/pedidos', require('./routers/pedidosRouter'))
app.use('/api/configuracion', require('./routers/configuracionRouter'))

const __dirnamePath = path.resolve()
//ruta pública para acceso a archivos
app.use('/uploads', express.static(path.join(__dirnamePath, '/uploads')))

app.listen(process.env.PORT, async () => {
    console.log(`Server up on http://localhost:${process.env.PORT}`)
    try {
        await sequelize.authenticate()
        console.log('Base de datos sincronizada!')
    } catch (error) {
        console.log('**** Error ****')
        console.error(error)
    }

});


