'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class soul_inv_grupo extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate({ Soul_ttt_cobros }) {
      this.hasMany(Soul_ttt_cobros, { foreignKey: 'id_categoria', as: 'cobros' })
    }
  };
  soul_inv_grupo.init({
    id_grupo: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    codigo: DataTypes.STRING(10),
    nombre: {
      type: DataTypes.STRING(100),
      allowNull: false,
      defaultValue: 'Sin Nombre'
    },
    descripcion: {
      type: DataTypes.STRING(2000),
      allowNull: false,
      defaultValue: 'Sin descripcion'
    },
    image: DataTypes.STRING,

  }, {
    sequelize,
    modelName: 'Soul_inv_grupo',
    tableName: 'soul_inv_grupos',
    timestamps: false
  });
  return soul_inv_grupo;
};