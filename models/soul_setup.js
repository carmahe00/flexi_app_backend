'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class soul_setup extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  soul_setup.init({
    Nit: {
      allowNull: false,
      primaryKey: true,
      type: DataTypes.STRING(20)
    },
    nombre: {
      type: DataTypes.STRING(100),
    },
    direccion: {
      type: DataTypes.STRING(100),
    },
    telefono: {
      type: DataTypes.STRING(100),
    },
    imagen: {
      type: DataTypes.STRING(200),
    }
  }, {
    sequelize,
    modelName: 'Soul_setup',
    tableName: 'soul_setup',
    timestamps: false
  });
  return soul_setup;
};