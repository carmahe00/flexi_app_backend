'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class soul_servicios_puesto extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  soul_servicios_puesto.init({
    id_puesto: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    id_servicio: {
      type: DataTypes.INTEGER,
      defaultValue: 0
    },
    Nombre: {
      type: DataTypes.STRING(45)
    },
    Estado: {
      type: DataTypes.INTEGER,
      defaultValue: 2
    },
    servicio: {
      type: DataTypes.STRING(100)
    },
    codigo: {
      type: DataTypes.STRING(45)
    },
    descripcion: {
      type: DataTypes.STRING(2000)
    },
    cargar: {
      type: DataTypes.INTEGER,
      defaultValue: 2
    }
  }, {
    sequelize,
    modelName: 'Soul_servicios_puesto',
    tableName: 'soul_servicios_puestos',
    timestamps: false
  });
  return soul_servicios_puesto;
};