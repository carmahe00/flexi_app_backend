'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class soul_ttt_cobros extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate({ Soul_inv_grupo }) {
      this.belongsTo(Soul_inv_grupo, { foreignKey: 'id_categoria', as: 'categoria' })
    }
  };
  soul_ttt_cobros.init({
    id_cobro: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    codigo: {
      type: DataTypes.STRING(20),
    },
    descripcion: {
      type: DataTypes.STRING(2000),
    },
    nombre: {
      type: DataTypes.STRING(100),
    },
    valor: {
      type: DataTypes.DECIMAL(15, 2)
    },
    total: {
      type: DataTypes.DECIMAL(15, 2)
    },
    impuesto: {
      type: DataTypes.DECIMAL(15, 2)
    },
    precio1: {
      type: DataTypes.DECIMAL(15, 2)
    },
    inicial: {
      type: DataTypes.DECIMAL(15, 2)
    },
    stok: {
      type: DataTypes.DECIMAL(15, 2)
    },
    inventario: {
      type: DataTypes.INTEGER,
      defaultValue: 1
    },
    image: DataTypes.STRING,
  }, {
    sequelize,
    modelName: 'Soul_ttt_cobros',
    tableName: 'soul_ttt_cobros',
    timestamps: false
  });
  return soul_ttt_cobros;
};