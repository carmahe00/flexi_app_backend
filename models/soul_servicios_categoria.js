'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class soul_servicios_categoria extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  soul_servicios_categoria.init({
    id_categoria: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    codigo: DataTypes.STRING(10),
    nombre: {
      type: DataTypes.STRING(100),
      defaultValue: 'Sin Nombre'
    },
    descripcion: {
      type: DataTypes.STRING(2000),
      defaultValue: 'Sin descripción'
    }
  }, {
    sequelize,
    modelName: 'Soul_servicios_categoria',
    tableName: 'soul_servicios_categorias',
    timestamps: false
  });
  return soul_servicios_categoria;
};